[🇬🇧](./README.md) [🇫🇷](./README_fr.md)

# Documentation

Welcome to the duniter documentation. You will find the most up-to-date **technical information** about the software in english and french. For more information about the duniter ecosystem or for more **user-friendly** documentation, please go to the [duniter website](https://duniter.org/en/).

## Installation

You have several methods to install duniter software:

- for the **official pre-built binaries**, go to the [Release page](https://git.duniter.org/nodes/typescript/duniter/-/releases)
- for **manual compilation**, go to the [detailed instructions](./manual_compilation.md) (only GNU/Linux)
- for a **docker container**, go to the [docker manual](./docker.md)
- for a **Windows install**, go to the [dedicated Windows page](#) TODO

## Specifications

The Duniter specifications are available in the [RFC](https://git.duniter.org/nodes/common/doc/blob/master/rfc). DUniter Protocol (DUP) is composed of the DUniter Blockchain Protocol (DUBP) and the DUniter Network Protocol (DUNP).

- [RFC 0009: Duniter Blockchain Protocol V11](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md)
- [RFC 0004: Duniter Network Protocol V1](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)

You can also read the [Duniter HTTP API (BMA)](./HTTP_API.md) page.

## Contribute

If you want to contribute technically to Duniter, please read the [developer documentation](./dev) which contains information about conventions, develop environment, architecture of the software...